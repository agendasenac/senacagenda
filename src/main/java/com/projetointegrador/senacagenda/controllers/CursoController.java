package com.projetointegrador.senacagenda.controllers;


import com.projetointegrador.senacagenda.models.CadastroCurso;
import com.projetointegrador.senacagenda.service.CursoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;


@Controller
public class CursoController {

    @Autowired
    private CursoService cursoService;

    @RequestMapping("/cadastroDeCurso")
    public String cadastroDeCurso() {

        return ("/cadastroCurso");
    }

    @PostMapping("/cadastrarCurso")
    public String cadastrarCurso(@Valid CadastroCurso cadastroCurso, BindingResult result, RedirectAttributes redirectAttributes) {
        String mensagem = "";
        if (result.hasErrors()) {
            if (result.toString().contains("O campo OFERTA não pode estar em branco")) {
                mensagem = mensagem + "O campo OFERTA não pode estar em branco";
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
            if (result.toString().contains("O campo NOME não pode estar em branco")) {
                mensagem = mensagem + " O campo NOME não pode estar em branco";
                System.out.println(result.toString());
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
            if (result.toString().contains("O campo CARGA HORARIA não pode estar em branco")) {
                mensagem = mensagem + " O campo CARGA HORARIA não pode estar em branco";
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
            if (result.toString().contains("O campo TURNO não pode estar em branco")) {
                mensagem = mensagem + " O campo TURNO não pode estar em branco";
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
        } else {
            try {
                cursoService.save(cadastroCurso);
                redirectAttributes.addFlashAttribute("mensagemEdicao", "Cadastro realizado com sucesso!");
            } catch (DataIntegrityViolationException ex) {
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", "Oferta de curso já cadastrada!");
            }
        }
        return "redirect:/listagemCurso";

    }

    @RequestMapping("/listagemCurso")
    public ModelAndView telaInicial() {
        ModelAndView modelAndView = new ModelAndView("listagemCurso");
        Iterable<CadastroCurso> cursos = cursoService.findAll();
        modelAndView.addObject("cursos", cursos);
        return modelAndView;
    }

    @GetMapping("/add")
    public ModelAndView add(CadastroCurso cadastroCurso) {
        ModelAndView modelAndView = new ModelAndView("editarCurso");
        modelAndView.addObject("curso", cadastroCurso);
        return modelAndView;
    }

    @GetMapping("/editarCurso/{id}")
    public ModelAndView editarCurso(@PathVariable("id") long id) {
        return add(cursoService.findOne(id));
    }

    @PostMapping("/salvarEdicaoCurso/{id}")
    public String salvarEdicaoCurso(@PathVariable("id") long id, @Valid CadastroCurso cadastroCurso, BindingResult result, RedirectAttributes redirectAttributes) {
        String mensagem = "";
        if (result.hasErrors()) {
            if (result.toString().contains("O campo OFERTA não pode estar em branco")) {
                mensagem = mensagem + "O campo OFERTA não pode estar em branco";
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
            if (result.toString().contains("O campo NOME não pode estar em branco")) {
                mensagem = mensagem + " O campo NOME não pode estar em branco";
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
            if (result.toString().contains("O campo CARGA HORARIA não pode estar em branco")) {
                mensagem = mensagem + " O campo CARGA HORARIA não pode estar em branco";
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
            if (result.toString().contains("O campo TURNO não pode estar em branco")) {
                mensagem = mensagem + " O campo TURNO não pode estar em branco";
                redirectAttributes.addFlashAttribute("mensagemFinalizacao", mensagem);
            }
        } else {
            cadastroCurso.setId(id);
            cursoService.save(cadastroCurso);
            redirectAttributes.addFlashAttribute("mensagemEdicao", "Edição realizada com sucesso!");
        }
        return "redirect:/listagemCurso";
    }

    @GetMapping("/finalizarCurso/{id}")
    public String finalizarCurso(@PathVariable("id") long id, CadastroCurso cadastroCurso, BindingResult result, RedirectAttributes redirectAttributes) {
        cadastroCurso = cursoService.findOne(id);
        cadastroCurso.setInativo(true);
        cadastroCurso.setId(id);
        cursoService.save(cadastroCurso);
        redirectAttributes.addFlashAttribute("mensagemFinalizacao", "Curso Finalizado!");
        return "redirect:/listagemCurso";
    }


}
