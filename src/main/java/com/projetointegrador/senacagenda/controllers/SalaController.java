package com.projetointegrador.senacagenda.controllers;


import com.projetointegrador.senacagenda.models.CadastroSala;
import com.projetointegrador.senacagenda.service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;


@Controller
public class SalaController {

    @Autowired
    private SalaService SalaService;

    @RequestMapping("/cadastroDeSala")
    public String cadastroDeSala() {
        return ("/cadastroSala");
    }

    @PostMapping("/cadastrarSala")
    public String cadastrarSala(@Valid CadastroSala cadastroSala, BindingResult result, RedirectAttributes redirectAttributes) {
        String mensagem = "";
        System.out.println(result.toString());
        if (result.hasErrors()) {
            if ((result.toString().contains("O campo NOME não pode estar em branco")) || (result.toString().contains("O campo BLOCO não pode estar em branco"))
                    || (result.toString().contains("O campo CAPACIDADE não pode estar em branco")) || (result.toString().contains("O campo CATEGORIA não pode estar em branco"))
                    || (result.toString().contains("O campo DESCRIÇÃO não pode estar em branco")) || (result.toString().contains("typeMismatch")))
            {
                mensagem = mensagem + " Não é aceito campos em branco";
                System.out.println(result.toString());
                redirectAttributes.addFlashAttribute("mensagemCadastroErro", mensagem);
            }
        } else {
            SalaService.save(cadastroSala);
            redirectAttributes.addFlashAttribute("mensagemCadastro", "Cadastro realizado com sucesso!");
        }
        return "redirect:/cadastroDeSala";
    }

    @RequestMapping("/listagemSala")
    public ModelAndView telaInicial() {
        ModelAndView modelAndView = new ModelAndView("listagemSala");
        Iterable<CadastroSala> salas = SalaService.findAll();
        modelAndView.addObject("salas", salas);
        return modelAndView;
    }

    @GetMapping("/addSala")
    public ModelAndView addSala(CadastroSala cadastroSala) {
        ModelAndView modelAndView = new ModelAndView("editarSala");
        modelAndView.addObject("sala", cadastroSala);
        return modelAndView;
    }

    @GetMapping("/editarSala/{id}")
    public ModelAndView editarSala(@PathVariable("id") long id) {
        return addSala(SalaService.findOne(id));
    }

    @PostMapping("/salvarEdicaoSala/{id}")
    public String salvarEdicaoSala(@PathVariable("id") long id, @Valid CadastroSala cadastroSala, BindingResult result, RedirectAttributes redirectAttributes) {
        String mensagem = "";
        System.out.println(result.toString());
        if (result.hasErrors()) {
            if ((result.toString().contains("O campo NOME não pode estar em branco")) || (result.toString().contains("O campo BLOCO não pode estar em branco"))
                    || (result.toString().contains("O campo CAPACIDADE não pode estar em branco")) || (result.toString().contains("O campo CATEGORIA não pode estar em branco"))
                    || (result.toString().contains("O campo DESCRIÇÃO não pode estar em branco")) || (result.toString().contains("typeMismatch")))
            {
                mensagem = mensagem + " Não é aceito campos em branco";
                System.out.println(result.toString());
                redirectAttributes.addFlashAttribute("mensagemCadastroErro", mensagem);
            }
        } else {
            cadastroSala.setId(id);
            SalaService.save(cadastroSala);
            redirectAttributes.addFlashAttribute("mensagemEdicao", "Edição realizada com sucesso!");
        }
        return "redirect:/listagemSala";
    }

    @GetMapping("/excluirSala/{id}")
    public String excluirSala(@PathVariable("id") long id, CadastroSala cadastroSala, RedirectAttributes redirectAttributes) {
        cadastroSala = SalaService.findOne(id);
        cadastroSala.setInativo(true);
        cadastroSala.setId(id);
        SalaService.save(cadastroSala);

        redirectAttributes.addFlashAttribute("mensagemExclusao", "Sala excluída com sucesso!");
        return "redirect:/listagemSala";
    }


}
