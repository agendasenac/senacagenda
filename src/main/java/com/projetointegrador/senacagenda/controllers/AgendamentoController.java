package com.projetointegrador.senacagenda.controllers;

import com.projetointegrador.senacagenda.models.Agendamento;
import com.projetointegrador.senacagenda.models.CadastroCurso;
import com.projetointegrador.senacagenda.models.CadastroSala;
import com.projetointegrador.senacagenda.service.AgendamentoService;
import com.projetointegrador.senacagenda.service.CursoService;
import com.projetointegrador.senacagenda.service.SalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AgendamentoController {

    @Autowired
    private AgendamentoService agendamentoService;

    @Autowired
    private CursoService cursoService;

    @Autowired
    private SalaService salaService;

    CadastroSala cadastroSala;


    @GetMapping("/agendamento")
    public String homeAgendamento() {
        return ("buscarCurso");
    }


    @PostMapping("/salvarAgendamento")
    public String salvarEdicaoCurso( Agendamento agendamento, BindingResult result, RedirectAttributes redirectAttributes){
        agendamentoService.save(agendamento);
        redirectAttributes.addFlashAttribute("agendamentoCadastrado", "Agendamento cadastrado com sucesso!");
        return "redirect:/agendamento";

        }
    @GetMapping("/addOferta")
    public ModelAndView addOferta(CadastroCurso cadastroCurso, long ofertaCurso) {
        ModelAndView modelAndView = new ModelAndView("agendamentoPeriodo");
        modelAndView.addObject("curso", cadastroCurso);
        Iterable<CadastroSala> salas = salaService.findAll();
        modelAndView.addObject("salas", salas);
        return modelAndView;
    }

        @GetMapping("/buscarCurso")
        public ModelAndView buscarCurso(@RequestParam(name = "ofertaCurso", required=false) long ofertaCurso) {
            System.out.println(ofertaCurso);
            return addOferta(cursoService.findOneByOfertaCurso(ofertaCurso), ofertaCurso);
        }
    }

