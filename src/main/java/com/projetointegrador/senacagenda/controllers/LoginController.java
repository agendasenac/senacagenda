package com.projetointegrador.senacagenda.controllers;
import com.projetointegrador.senacagenda.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @Autowired
    private LoginRepository loginRepository;

    @GetMapping("/login")
    public String realizarLogin() {
        return ("login");
    }

}
