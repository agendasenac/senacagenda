package com.projetointegrador.senacagenda.service;

import com.projetointegrador.senacagenda.models.CadastroSala;
import com.projetointegrador.senacagenda.repository.CadastroSalaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class SalaService {

    @Autowired
    private CadastroSalaRepository cadastroSalaRepository;

    public  Iterable<CadastroSala> findAll() {
        return cadastroSalaRepository.findAll();
    }

    public CadastroSala findOne(long id){
        return cadastroSalaRepository.findById(id);
    }

    public CadastroSala save (CadastroSala cadastroSala){
        return cadastroSalaRepository.save(cadastroSala);
    }

    public void delete(long id){
        cadastroSalaRepository.deleteById(id);
    }
}
