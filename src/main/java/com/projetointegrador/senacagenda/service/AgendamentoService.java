package com.projetointegrador.senacagenda.service;


import com.projetointegrador.senacagenda.controllers.AgendamentoController;
import com.projetointegrador.senacagenda.models.Agendamento;
import com.projetointegrador.senacagenda.models.CadastroCurso;
import com.projetointegrador.senacagenda.repository.AgendamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AgendamentoService {

    @Autowired
    private AgendamentoRepository agendamentoRepository;


    public  Iterable<Agendamento> findAll() {
        return agendamentoRepository.findAll();
    }

    public Agendamento findOne(long idAgendamento){
        return agendamentoRepository.findByIdAgendamento(idAgendamento);
    }

    public Agendamento save (Agendamento agendamento){
        return agendamentoRepository.save(agendamento);
    }

    public void delete(long idAgendamento){
        agendamentoRepository.deleteByIdAgendamento(idAgendamento);
    }
}
