package com.projetointegrador.senacagenda.service;

import com.projetointegrador.senacagenda.models.CadastroCurso;
import com.projetointegrador.senacagenda.repository.CadastroCursoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;


@Service
@Transactional
public class CursoService {

    @Autowired
    private CadastroCursoRepository cadastroCursoRepository;

    public  Iterable<CadastroCurso> findAll() {
    return cadastroCursoRepository.findAll();
}

    public CadastroCurso findOne(long id){
        return cadastroCursoRepository.findById(id);
    }

    public CadastroCurso findOneByOfertaCurso(long ofertaCurso){
        return cadastroCursoRepository.findByOfertaCurso(ofertaCurso);
    }

    public CadastroCurso save (CadastroCurso cadastroCurso){
        return cadastroCursoRepository.save(cadastroCurso);
    }

    public void delete(long id){
        cadastroCursoRepository.deleteById(id);
    }
}
