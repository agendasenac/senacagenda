package com.projetointegrador.senacagenda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class SenacagendaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SenacagendaApplication.class, args);
		System.out.println(new BCryptPasswordEncoder().encode("123")); //senha gerada que deve ser inserida na base de dados OBS: precisa ser realizado o insert na base de dados do usuario e a senha gerada
	}

}