package com.projetointegrador.senacagenda.models;

import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class CadastroSala {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    @NotBlank(message = "{nomeSala.not.blank}")
    private String nomeSala;
    @NotBlank(message = "{blocoSala.not.blank}")
    private String blocoSala;
    @Range(min = 1, max = 200)
    private int capacidadeSala;
    @NotBlank(message = "{categoriaSala.not.blank}")
    private String categoriaSala;
    @NotBlank(message = "{descricaoSala.not.blank}")
    private String descricaoSala;
    private boolean inativo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNomeSala() {
        return nomeSala;
    }

    public void setNomeSala(String nomeSala) {
        this.nomeSala = nomeSala;
    }

    public String getBlocoSala() {
        return blocoSala;
    }

    public void setBlocoSala(String blocoSala) {
        this.blocoSala = blocoSala;
    }

    public int getCapacidadeSala() {
        return capacidadeSala;
    }

    public void setCapacidadeSala(int capacidadeSala) {
        this.capacidadeSala = capacidadeSala;
    }

    public String getCategoriaSala() {
        return categoriaSala;
    }

    public void setCategoriaSala(String categoriaSala) {
        this.categoriaSala = categoriaSala;
    }

    public String getDescricaoSala() {
        return descricaoSala;
    }

    public void setDescricaoSala(String descricaoSala) {
        this.descricaoSala = descricaoSala;
    }

    public boolean isInativo() {
        return inativo;
    }

    public void setInativo(boolean inativo) {
        this.inativo = inativo;
    }
}
