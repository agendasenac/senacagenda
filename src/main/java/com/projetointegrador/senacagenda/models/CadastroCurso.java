package com.projetointegrador.senacagenda.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class CadastroCurso {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    @Column(unique = true)
    @NotNull(message = "{ofertaCurso.not.blank}")
    private long ofertaCurso;
   @NotBlank(message = "{nomeCurso.not.blank}")
    private String nomeCurso;
    @NotNull(message = "{cargaHoraria.not.blank}")
    private int cargaHoraria;
    @NotBlank(message = "{turno.not.blank}")
    private String turno;
    private boolean inativo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getOfertaCurso() {
        return ofertaCurso;
    }

    public void setOfertaCurso(long ofertaCurso) {
        this.ofertaCurso = ofertaCurso;
    }

    public String getNomeCurso() {
        return nomeCurso;
    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public boolean isInativo() {
        return inativo;
    }

    public void setInativo(boolean inativo) {
        this.inativo = inativo;
    }
}
