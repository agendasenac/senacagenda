package com.projetointegrador.senacagenda.repository;


import com.projetointegrador.senacagenda.models.Agendamento;
import org.springframework.data.repository.CrudRepository;

public interface AgendamentoRepository extends CrudRepository<Agendamento, String>{

    Agendamento findByIdAgendamento (long idAgendamento);
    public void deleteByIdAgendamento(long idAgendamento);
}
