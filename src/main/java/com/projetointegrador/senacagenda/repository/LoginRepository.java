package com.projetointegrador.senacagenda.repository;

import com.projetointegrador.senacagenda.models.Login;
import org.springframework.data.repository.CrudRepository;

public interface LoginRepository extends CrudRepository<Login, String> {

    Login findByUsername(String username);
}
