package com.projetointegrador.senacagenda.repository;

import com.projetointegrador.senacagenda.models.CadastroCurso;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CadastroCursoRepository extends CrudRepository<CadastroCurso, String> {

    CadastroCurso findById(long id);
    CadastroCurso findByOfertaCurso(long ofertaCurso);
    void deleteById(long id);

    @Override
    @Query("select u from CadastroCurso u where u.inativo = 0")
    Iterable<CadastroCurso> findAll();
}
