package com.projetointegrador.senacagenda.repository;

import com.projetointegrador.senacagenda.models.CadastroSala;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CadastroSalaRepository extends CrudRepository<CadastroSala, String> {

    CadastroSala findById(long id);
    public void deleteById(long id);

    @Override
    @Query("select u from CadastroSala u where u.inativo = 0")
    Iterable<CadastroSala> findAll();
}
