package com.projetointegrador.senacagenda.security;

import com.projetointegrador.senacagenda.models.Login;
import com.projetointegrador.senacagenda.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

/**
 * @author s2it_tpereira
 * @since 9/13/19 8:08 PM
 */
@Repository
public class ImplementsUserDetailsService implements UserDetailsService {

    @Autowired
    private LoginRepository loginRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Login login = loginRepository.findByUsername(username);
        if (username == null){
            throw new UsernameNotFoundException("Usuário não encontrado!");
        }
        return login;
    }
}
