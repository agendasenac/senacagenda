package com.projetointegrador.senacagenda.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private ImplementsUserDetailsService userDetailsService;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.csrf().disable().authorizeRequests() //recursos do HTTP para prover vários tipos de segurança para a aplicação. Desabilitando por enquanto
                .antMatchers(HttpMethod.GET, "/").permitAll() //Significa que quando o usuário acessar a index, não irá solicitar senha
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login").permitAll() //Qualquer outra página, irá ser solicitado o login
                .and().formLogin().defaultSuccessUrl("/", true)
                .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")); //Será encerrada a sessão

    }

    @Override
    protected void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception{
        authenticationManagerBuilder.userDetailsService(userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder()); //gerar uma senha criptografada
    }

    @Override
    public void configure(WebSecurity webSecurity) throws Exception{
        webSecurity.ignoring().antMatchers("/css/**", "/img/**", "/js/**"); //Spring security não irá bloquear as páginas estáticas
    }
}
